package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements  UserService{

    @Autowired
    private UserRepository userRepository;

    //Find user by username
    @Override
    //if findByUsername method returns null it will throw a NullPointerException.
    //using .ofNullable method will avoid this from happening.

    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepository.findByUsername(username));
    }


    @Override
    public void createUser(User user) {
        userRepository.save(user);
    }

    @Override
    public Iterable getUsers() {
        return userRepository.findAll();
    }

    @Override
    public ResponseEntity updateUser(Long userid, User user) {
        User userUpdate = userRepository.findById(userid).get();

        userUpdate.setUsername(user.getUsername());
        userUpdate.setPassword(user.getPassword());

        userRepository.save(userUpdate);

        return new ResponseEntity("User updated successfully.", HttpStatus.OK);
    }

    @Override
    public ResponseEntity deleteUser(Long userid) {
        userRepository.deleteById(userid);
        return new ResponseEntity("User deleted successfully.", HttpStatus.OK);
    }
}
