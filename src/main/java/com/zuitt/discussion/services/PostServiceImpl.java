//Contains the business logic concerned with a particular object in the class.
package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public class PostServiceImpl implements PostService{

    @Autowired
    PostRepository postRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    @Override
    public void createPost(String stringToken, Post post) {

        //Retrieve the "User" object using the extracted username from the JWT Token.
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));


        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);

        postRepository.save(newPost);

    }

    @Override
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    @Override
    public ResponseEntity deletePost(Long postid, String stringToken) {

        Post postForDeleting = postRepository.findById(postid).get();
        String postAuthorName = postForDeleting.getUser().getUsername();
        String authenticatedUsername = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUsername.equals(postAuthorName)) {
            postRepository.deleteById(postid);
            return new ResponseEntity("Post deleted successfully", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }
    }



   @Override
    public ResponseEntity updatePost(Long postid, String stringToken, Post post) {
        /*Post postForUpdate = postRepository.findById(postid).get();

        postForUpdate.setTitle((post.getTitle()));
        postForUpdate.setContent(post.getContent());

        postRepository.save(postForUpdate);

        return new ResponseEntity("Post updated successfully", HttpStatus.OK);*/


       Post postForUpdating = postRepository.findById(postid).get();

       //Get the "author of the specific post.
       String postAuthorName = postForUpdating.getUser().getUsername();

       //Get the "username" from the stringToken to compare it with the username of the post being edited.
       String authenticatedUsername = jwtToken.getUsernameFromToken(stringToken);

       //Checks if the username of the authenthicated user matches
       if(authenticatedUsername.equals(postAuthorName)){
           postForUpdating.setTitle(post.getTitle());
           postForUpdating.setContent(post.getContent());
           postRepository.save(postForUpdating);

           return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
       }
       else{
           return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
       }
    }


    //Get user's posts
    public Iterable<Post> getMyPosts(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getPosts();
    }


}
