package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {

    //Optional - defines if the method may/may not return an object of the User class.
    Optional<User> findByUsername(String username);

    void createUser(User user);

    Iterable getUsers();

    ResponseEntity updateUser (Long userid, User user);

    ResponseEntity deleteUser (Long userid);
}
